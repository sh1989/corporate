#!/bin/sh
#
# Builds NE-RPC assets, styles and documents

echo 'Cleaning up...'
rm -rf public
mkdir -p public

echo 'Generating assets...'
cp -r config/* public/
cp -r styles/* public/
cp -r assets/* public/

node build.js --unhandled-rejections=strict
