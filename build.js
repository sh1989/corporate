const fs = require('fs');
const Mustache = require('mustache');
const path = require('path');
const util = require('util');

const encoding = 'utf8';

const file = {
  readdir: util.promisify(fs.readdir),
  readFile: util.promisify(fs.readFile),
  writeFile: util.promisify(fs.writeFile)
};

async function readData() {
  const data = await file.readFile(path.join("data", "data.json"), { encoding });
  return JSON.parse(data);
}

async function readPartials() {
  const folder = "partials";

  const files = await file.readdir(folder);
  const partials = await Promise.all(
    files.map(f => file.readFile(path.join(folder, f), encoding)
      .then(data => ({
        title: f.split('.').slice(0, -1).join('.'),
        data
      }))
    )
  );
  return partials.reduce((agg, i) => {
    agg[i.title] = i.data;
    return agg;
  }, {});
}

async function renderIndex(data, partials) {
  const template = await file.readFile('index.mustache', { encoding });
  const output = Mustache.render(template, data, partials);
  console.log(output);
  await file.writeFile(path.join('public', 'index.html'), output, { encoding });
}

async function main() {
  const partials = await readPartials();
  const data = await readData();
  renderIndex(data, partials);
}

main();